# ChatApp

## Instructions

Install dependencies

```
npm install
```

Run server

```
ionic serve
```

## Important links

* https://ionicframework.com/docs/
* https://ionicframework.com/docs/native/firebase/
* https://medium.com/learn-ionic-framework/construye-una-app-con-ionic-y-firebase-paso-a-paso-218105b77263

# Authors

* Azael Rodríguez - (Developer, Designer) [[Gitlab](https://gitlab.com/azael_rguez)]
* Salvador Quintero - (Developer, Documentation) [[Gitlab](https://gitlab.com/Shavatl)]

## Licence

This project is licensed under the Apache-2.0 License - see the [LICENSE](LICENSE) file for details.
