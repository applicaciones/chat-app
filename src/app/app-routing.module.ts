import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './user/login/login.component';
import { SignupComponent } from './user/signup/signup.component';
import { UserLogComponent } from './user-log/user-log.component';
import { ChatComponent } from './user-log/chat/chat.component';
import { SettingsComponent } from './user-log/settings/settings.component';
import { OptionsComponent } from './user-log/settings/options/options.component';
import { AboutusComponent } from './user-log/settings/aboutus/aboutus.component';
import { LanguageComponent } from './user-log/settings/language/language.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'panel', component: UserLogComponent, children: [
    { path: 'chat', component: ChatComponent },
    { path: 'settings', component: SettingsComponent, 
      children: [
      { path: 'options', component: OptionsComponent },
      { path: 'aboutus', component: AboutusComponent },
      { path: 'language', component: LanguageComponent }
    ]}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const RoutingComponents = [
  HomeComponent,
  LoginComponent,
  SignupComponent,
  UserLogComponent,
  ChatComponent,
  SettingsComponent,
  OptionsComponent,
  AboutusComponent,
  LanguageComponent
];