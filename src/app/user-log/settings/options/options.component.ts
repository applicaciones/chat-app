import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth, public router: Router,
    public toastController: ToastController) { }

  ngOnInit() {}

  async presentAlert(content: string) {
		const toast = await this.toastController.create({
      message: content,
      duration: 2000
    });
    toast.present();
	}

  logout() {
    try {
      this.afAuth.auth.signOut();
      this.router.navigate(['/home']);
    } catch(error) {
      console.log(error);
      this.presentAlert('Algo salió mal.');
    }
    
  }

}
