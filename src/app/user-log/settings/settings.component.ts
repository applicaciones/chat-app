import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth, public router: Router,
    public toastController: ToastController, public alertController: AlertController) { }

  ngOnInit() {}


  async showInfo() {
    console.log("skjdfhsk");   
    const alert = await this.alertController.create({
      header: 'Acerca de',
      subHeader: 'chatapp alpha 1.0.0',
      message: 'Desarrolladores: Salvador Quintero y Azael Rodríguez.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlert(content: string) {
		const toast = await this.toastController.create({
      message: content,
      duration: 2000
    });
    toast.present();
  }
  
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: '¿Quieres cerrar la sesión?',
      message: '',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {}
        }, {
          text: 'Ok',
          handler: () => {
            try {
              this.afAuth.auth.signOut();
              this.router.navigate(['/home']);
            } catch(error) {
              console.log(error);
              this.presentAlert('Algo salió mal.');
            }
          }
        }
      ]
    });

    await alert.present();
  }

  logout() {
    /* try {
      this.afAuth.auth.signOut();
      this.router.navigate(['/home']);
    } catch(error) {
      console.log(error);
      this.presentAlert('Algo salió mal.');
    } */
    this.presentAlertConfirm();
  }

}
