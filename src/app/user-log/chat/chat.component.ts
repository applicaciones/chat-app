import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChatComponent implements OnInit {

  messageme:string;
  htmlMessage:string="";
  responseMessage:string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer d59bf035e38d4c8bbe7557bfa92753f8'
    })
  };

  constructor(private http: HttpClient,
    public toastController: ToastController) { }

  ngOnInit() { }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Ha habido un error.',
      duration: 2000
    });
    toast.present();
  }

  messagemeShow() {
    if (this.messageme){
      this.htmlMessage += "<div class='blubble me'><p>"+this.messageme+"</p></div>";
      this.reqMessage();
      this.messageme = "";
    }
  }

  reqMessage() {
    try {
      this.http.post('https://api.dialogflow.com/v1/query?v=20150910', {
        "lang": "es",
        "query": this.messageme,
        "sessionId": "12345",
        "timezone": "America/New_York"
      }, this.httpOptions).subscribe(
        data => {
          this.responseMessage = data["result"]["fulfillment"]["speech"];
          this.htmlMessage += "<div class='blubble bot'><p>"+this.responseMessage+"</p></div>";
        },
        err => { }
      );
    }catch(err) {
      this.presentToast();
    } 
  }

}
