import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(
    public afAuth: AngularFireAuth,
		public afstore: AngularFirestore,
		public toastController: ToastController,
    public router: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {}

  async presentAlertError(content: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: content,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlert(content: string) {
		const toast = await this.toastController.create({
      message: content,
      duration: 2000
    });
    toast.present();
	}

  async loginGoogle() {
    try {
      const res = await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
      console.log(res)
      this.router.navigate(['/panel/chat']);
    } catch(error) {
      console.log(error);
      this.presentAlertError(error);
      this.presentAlert('Algo salió mal.');
    }
  }

}
