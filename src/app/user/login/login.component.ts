import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  email: string = "";
  password: string = "";

  constructor(public afAuth: AngularFireAuth, public router: Router,
    public toastController: ToastController){}
  ngOnInit() {}

  async presentAlert(content: string) {
		const toast = await this.toastController.create({
      message: content,
      duration: 2000
    });
    toast.present();
	}

  async login() {
    const { email, password } = this;
    try {
      const res = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      this.router.navigate(['/panel/chat']);
    } catch(err) {
      console.log(err)
      this.presentAlert("Ups! Datos inválidos.");
    }
  }

}
