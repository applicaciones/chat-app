import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {

  /* 
    si los datos no son correctos, poner un Toast
    https://ionicframework.com/docs/api/toast
    de lo contrario, pasar a la vista de Login
  */

  username: string = "";
  email: string = "";
  password: string = "";
  cpassword: string = "";

  constructor(
    public afAuth: AngularFireAuth,
		public afstore: AngularFirestore,
		public toastController: ToastController,
		public router: Router
  ) { }

  ngOnInit() {}

  async presentAlert(content: string) {
		const toast = await this.toastController.create({
      message: content,
      duration: 2000
    });
    toast.present();
	}

  async signup() {
    const { username, email, password, cpassword } = this;
		if(password !== cpassword) {
			this.presentAlert('Las contraseñas no coinsiden.')
		} else {
      try {
        const res = await this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        this.presentAlert('Se ha registrado con éxito.');
        this.router.navigate(['/login']);
      } catch(error) {
        this.presentAlert('Algo salió mal.');
      }
    }
  }

}
